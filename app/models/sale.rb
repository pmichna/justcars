class Sale < ApplicationRecord
  SLUG_MAX_LENGTH = 60
  SLUG_RANDOM_PART_LENGTH = 10

  has_one_attached :photo
  
  validates :title, presence: true
  validates :price, numericality: { only_integer: true, greater_than_or_equal_to: 0 }
  validates :slug, presence: true, length: { maximum: SLUG_MAX_LENGTH }
end
