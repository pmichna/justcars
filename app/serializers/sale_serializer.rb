class SaleSerializer
  include FastJsonapi::ObjectSerializer
  attributes :title, :description, :slug, :price

  attribute :photo_path do |object|
    if object.photo.attached?
      Rails.application.routes.url_helpers.rails_blob_path(object.photo, only_path: true)
    else
      nil
    end
  end
end
