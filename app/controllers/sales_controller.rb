class SalesController < ApplicationController
  # GET /sales
  def index
    # TODO: adding sorting would be nice
    svc_filterer = SaleFilterer.new(params[:filter])
    filtered = svc_filterer.call.sales # TODO: not perfect - should check `success?``
    svc_pager = SalePager.new(filtered, params[:page])
    result = svc_pager.call

    if result.success?
      render json: SaleSerializer.new(result.sales).serializable_hash
    else
      render json: {errors: result.errors}, status: :bad_request
    end
  end

  # GET /sales/1
  def show
    sale = Sale.find_by(slug: params[:slug])
    render json: SaleSerializer.new(sale).serializable_hash
  end

  # POST /sales
  def create
    svc = SaleCreator.new(sale_params)
    result = svc.call
    if result.success?
      render json: SaleSerializer.new(result.sale).serializable_hash, status: :created, location: result.sale
    else
      render json: {errors: result.errors}, status: :unprocessable_entity
    end
  end

  private
    def sale_params
      params.require(:sale).permit(:title, :description, :price, :photo)
    end
end
