class SalePager < ApplicationService
  def initialize(sales, page_params)
    @sales = sales
    @page_params = page_params
  end

  def call
    sales = @sales

    if !@page_params.nil? && valid_params?
      page_number = @page_params[:number].to_i
      page_size = @page_params[:size].to_i
      
      sales = sales.offset((page_number-1)*page_size).limit(page_size)
    end
    
    if valid_params?
      OpenStruct.new(success?: true, sales: sales, errors: nil)
    else
      OpenStruct.new(success?: false, sales: [], errors: filter_errors)
    end
  end

  private

  def valid_params?
    true # TODO
  end

  def filter_errors
    [] # TODO
  end
end