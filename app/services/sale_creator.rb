class SaleCreator < ApplicationService
  def initialize(params)
    @params = params
  end

  def call
    sale = Sale.new(@params)
    slug_title_part_length = Sale::SLUG_MAX_LENGTH - Sale::SLUG_RANDOM_PART_LENGTH
    trimmed_title = @params[:title][0...slug_title_part_length]
    slug = trimmed_title + '-' + SecureRandom.alphanumeric(Sale::SLUG_RANDOM_PART_LENGTH)
    sale.slug = slug.downcase.gsub(' ', '-')
    if sale.save
      OpenStruct.new(success?: true, sale: sale, errors: nil)
    else
      OpenStruct.new(success?: false, sale: sale, errors: sale.errors)
    end
  end
end