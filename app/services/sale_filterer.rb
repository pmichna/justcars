class SaleFilterer < ApplicationService
  def initialize(filter_params)
    @filter_params = filter_params
  end

  def call
    sales = Sale.all

    if !@filter_params.nil? && valid_params?
      title_filter = @filter_params[:title]
      description_filter = @filter_params[:description]
      price_min_filter = @filter_params[:price_min]
      price_max_filter = @filter_params[:price_max]
      photo_filter = @filter_params[:photo]
      
      sales = sales.where('title ILIKE ?', "%#{title_filter}%") if title_filter
      sales = sales.where('description ILIKE ?', "%#{description_filter}%") if description_filter
      sales = sales.where('price >= ?', price_min_filter) if price_min_filter
      sales = sales.where('price <= ?', price_max_filter) if price_max_filter
      sales = sales.joins(:photo_attachment) if photo_filter == 'true'
    end
    
    if valid_params?
      OpenStruct.new(success?: true, sales: sales, errors: nil)
    else
      OpenStruct.new(success?: false, sales: [], errors: filter_errors)
    end
  end

  private

  def valid_params?
    true # TODO
  end

  def filter_errors
    [] # TODO
  end
end