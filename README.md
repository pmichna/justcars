# README

## Setup

 ```
$ docker-compose build

$ docker-compose up -d web db

$ docker-compose run web rails db:setup

$ docker-compose up tests

$ curl -H "Content-Type: application/json" http://localhost:3000/api/sales | jq # jeśli jest `jq`
```

## Znane problemy

1. Zwracanie błędów nie jest zgodne z [JSON:API](https://jsonapi.org/format/#errors).
1. Zwracany i przyjmowany `Content-Type` nie jest zgodny z [JSON:API](https://jsonapi.org/format/#content-negotiation).
1. Inne - oznaczone w kodzie jako `# TODO`.