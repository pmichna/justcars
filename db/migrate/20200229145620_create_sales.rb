class CreateSales < ActiveRecord::Migration[6.0]
  def change
    create_table :sales do |t|
      t.string :title, null: false, index: true
      t.string :slug, null: false, index: true, limit: 60
      t.text :description
      t.integer :price, null: false, index: true

      t.timestamps
    end
  end
end
