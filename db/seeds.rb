40.times do |i|
  title = "#{Faker::Vehicle.make_and_model} #{Faker::Vehicle.color} #{Faker::Vehicle.year}"
  puts "Seeding #{title}..."
  svc = SaleCreator.new({
    title: title,
    description: Faker::Lorem.paragraph,
    price: Random.rand(100_000)+2_000
  })
  sale = svc.call.sale
  photo = open(Rails.root.join('spec/fixtures/files/example_car.jpg'))
  sale.photo.attach(io: photo, filename: 'photo.jpg')
end