require 'rails_helper'

RSpec.describe SaleCreator do
  describe '#call' do
    let(:params) { {title: 'nice car', description: 'very good description', price: 9999} }
    subject { described_class.new(params).call }

    it 'creates a sale with a slug' do
      expect { subject }.to change { Sale.count }.by(1)
      sale = subject.sale
      expect(subject.success?).to eq(true)
      expect(sale.title).to eq(params[:title])
      expect(sale.description).to eq(params[:description])
      expect(sale.price).to eq(params[:price])
      expect(sale.slug).to be_present
    end

    it 'creates different slugs for two sales with the same attributes' do
      sale_1 = described_class.new(params).call.sale
      sale_2 = described_class.new(params).call.sale
      expect(sale_1.slug).not_to eq(sale_2.slug)
    end

    it 'creates a kebab-cased slug' do
      params_local = params
      params_local[:title] = 'a TiTle wItH SpAces'
      sale = subject.sale
      expect(sale.slug).to include('a-title-with-spaces')
    end

    it 'creates lower case slug' do
      params_local = params
      params_local[:title] = 'a TiTle wItH SpAces 123'
      sale = subject.sale
      expect(sale.slug.split('')).to all( match(/[a-z\-\d]/))
    end

    it 'creates slug > SLUG_RANDOM_PART_LENGTH <= SLUG_MAX_LENGTH' do
      expect(subject.sale.slug.length).to be > Sale::SLUG_RANDOM_PART_LENGTH
      expect(subject.sale.slug.length).to be <= Sale::SLUG_MAX_LENGTH
    end
  end
end
