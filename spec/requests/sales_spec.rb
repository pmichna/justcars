require 'rails_helper'

RSpec.describe 'Sales' do
  describe 'POST /api/sales' do
    subject { post '/api/sales', params: {sale: attributes} }

    context 'valid parameters' do
      let(:attributes) do
        {
          title: 'a very nice car',
          description: 'this is indeed a very nice car please buy',
          price: 10000,
          photo: fixture_file_upload('files/example_car.jpg', 'image/jpeg')
        }
      end

      it 'creates a Sale in database' do
        expect { subject }.to change { Sale.count }.by(1)
        sale = Sale.last
        expect(sale.title).to eq(attributes[:title])
        expect(sale.description).to eq(attributes[:description])
        expect(sale.price).to eq(attributes[:price])
        expect(sale.photo).to be_attached
      end

      it 'returns Sale attributes with HTTP created status' do
        subject
        json_attributes = JSON.parse(response.body)['data']['attributes']
        expect(json_attributes['title']).to eq(attributes[:title])
        expect(json_attributes['description']).to eq(attributes[:description])
        expect(json_attributes['slug']).to be_present
        expect(json_attributes['price']).to eq(attributes[:price])
        expect(response).to have_http_status(:created)
      end
    end

    context 'invalid parameters' do
      let(:attributes) { {title: '', price: ''} }

      it 'returns errors' do
        subject
        errors = JSON.parse(response.body)['errors']
        expect(errors).to eq(
          {
            "price" => ["is not a number"],
            "title" => ["can't be blank"]
          }
        )
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

  describe 'GET /api/sales' do
    it 'returns all sales' do
      10.times do |i|
        svc = SaleCreator.new({
          title: Faker::Vehicle.make_and_model,
          description: Faker::Lorem.paragraph,
          price: Random.rand(100_000)
        })
        sale = svc.call.sale
        photo = open(Rails.root.join('spec/fixtures/files/example_car.jpg'))
        sale.photo.attach(io: photo, filename: 'photo.jpg')
      end

      get '/api/sales'

      sales = Sale.all.map do |s|
        {
          "attributes" => { 
            "description" => s.description,
            "price" => s.price,
            "slug" => kind_of(String),
            "title" => s.title,
            "photo_path" => kind_of(String)
          },
          "type" => "sale",
          "id" => kind_of(String)
        }
      end
      expect(JSON.parse(response.body)['data']).to match_array(sales)
    end
  end

  describe 'GET /api/sales?page=x' do
    let!(:honda_sale) do
      SaleCreator.new({
        title: 'Honda accord 2009',
        description: 'Great Honda please buy',
        price: 1000
      }).call.sale
    end

    let!(:mondeo_sale) do
      SaleCreator.new({
        title: 'Ford Mondeo 2009',
        description: 'Good Mondeo recommend',
        price: 10000
      }).call.sale
    end      
  
    let!(:zafira_sale) do
      SaleCreator.new({
        title: 'Opel Zafira 2011',
        description: 'Nice family car',
        price: 20000
      }).call.sale  
    end

    let!(:polonez_sale) do
      SaleCreator.new({
        title: 'Polonez Caro 1996',
        description: 'Great for collections',
        price: 100000
      }).call.sale  
    end

    it 'returns first page' do
      get '/api/sales?page[number]=1&page[size]=2'

      expect(JSON.parse(response.body)['data']).to contain_exactly(
        {
          "attributes" => { 
            "description" => honda_sale.description,  
            "price" => honda_sale.price,
            "slug" => honda_sale.slug,
            "title" => honda_sale.title,
            "photo_path" => nil
          },
          "type" => "sale",
          "id" => honda_sale.id.to_s
        },
        {
          "attributes" => { 
            "description" => mondeo_sale.description,  
            "price" => mondeo_sale.price,
            "slug" => mondeo_sale.slug,
            "title" => mondeo_sale.title,
            "photo_path" => nil
          },
          "type" => "sale",
          "id" => mondeo_sale.id.to_s
        }
      )
    end
  end

  describe 'GET /api/sales?filter' do
    let!(:honda_sale) do
      SaleCreator.new({
        title: 'Honda accord 2009',
        description: 'Great Honda please buy',
        price: 1000
      }).call.sale
    end

    let!(:mondeo_sale) do
      SaleCreator.new({
        title: 'Ford Mondeo 2009',
        description: 'Good Mondeo recommend',
        price: 10000
      }).call.sale
    end      
  
    let!(:zafira_sale) do
      SaleCreator.new({
        title: 'Opel Zafira 2011',
        description: 'Nice family car',
        price: 20000
      }).call.sale  
    end
    
    it 'returns sales filtered by title' do
      get '/api/sales?filter[title]=honda'
      expect(JSON.parse(response.body)['data']).to contain_exactly(
        {
          "attributes" => { 
            "description" => honda_sale.description,  
            "price" => honda_sale.price,
            "slug" => honda_sale.slug,
            "title" => honda_sale.title,
            "photo_path" => nil
          },
          "type" => "sale",
          "id" => honda_sale.id.to_s
        }
      )
    end

    it 'returns sales filtered by description' do
      get '/api/sales?filter[description]=recommend'
      expect(JSON.parse(response.body)['data']).to contain_exactly(
        {
          "attributes" => { 
            "description" => mondeo_sale.description,  
            "price" => mondeo_sale.price,
            "slug" => mondeo_sale.slug,
            "title" => mondeo_sale.title,
            "photo_path" => nil
          },
          "type" => "sale",
          "id" => mondeo_sale.id.to_s
        }
      )
    end

    it 'returns sales filtered by price min' do
      get '/api/sales?filter[price_min]=9000'
      expect(JSON.parse(response.body)['data']).to contain_exactly(
        {
          "attributes" => { 
            "description" => mondeo_sale.description,  
            "price" => mondeo_sale.price,
            "slug" => mondeo_sale.slug,
            "title" => mondeo_sale.title,
            "photo_path" => nil
          },
          "type" => "sale",
          "id" => mondeo_sale.id.to_s
        },
        {
          "attributes" => { 
            "description" => zafira_sale.description,  
            "price" => zafira_sale.price,
            "slug" => zafira_sale.slug,
            "title" => zafira_sale.title,
            "photo_path" => nil
          },
          "type" => "sale",
          "id" => zafira_sale.id.to_s
        }
      )
    end

    it 'returns sales filtered by price max' do
      get '/api/sales?filter[price_max]=9000'
      expect(JSON.parse(response.body)['data']).to contain_exactly(
        {
          "attributes" => { 
            "description" => honda_sale.description,  
            "price" => honda_sale.price,
            "slug" => honda_sale.slug,
            "title" => honda_sale.title,
            "photo_path" => nil
          },
          "type" => "sale",
          "id" => honda_sale.id.to_s
        }
      )
    end

    it 'returns sales filtered by photo present' do
      photo_1 = open(Rails.root.join('spec/fixtures/files/example_car.jpg'))
      photo_2 = open(Rails.root.join('spec/fixtures/files/example_car.jpg'))
      honda_sale.photo.attach(io: photo_1, filename: 'photo.jpg')
      mondeo_sale.photo.attach(io: photo_2, filename: 'photo.jpg')
      
      get '/api/sales?filter[photo]=true'
      
      expect(JSON.parse(response.body)['data']).to contain_exactly(
        {
          "attributes" => { 
            "description" => mondeo_sale.description,  
            "price" => mondeo_sale.price,
            "slug" => mondeo_sale.slug,
            "title" => mondeo_sale.title,
            "photo_path" => Rails.application.routes.url_helpers.rails_blob_path(mondeo_sale.photo, only_path: true)
          },
          "type" => "sale",
          "id" => mondeo_sale.id.to_s
        },
        {
          "attributes" => { 
            "description" => honda_sale.description,  
            "price" => honda_sale.price,
            "slug" => honda_sale.slug,
            "title" => honda_sale.title,
            "photo_path" => Rails.application.routes.url_helpers.rails_blob_path(honda_sale.photo, only_path: true)
          },
          "type" => "sale",
          "id" => honda_sale.id.to_s
        }
      )
    end

    it 'returns sales filtered by all attributes' do
      photo = open(Rails.root.join('spec/fixtures/files/example_car.jpg'))
      mondeo_sale.photo.attach(io: photo, filename: 'photo.jpg')
      
      get '/api/sales?filter[photo]=true&filter[price_max]=10000&filter[title]=2009&filter[description]=recommend&filter[price_min]=100'

      expect(JSON.parse(response.body)['data']).to contain_exactly(
        {
          "attributes" => { 
            "description" => mondeo_sale.description,  
            "price" => mondeo_sale.price,
            "slug" => mondeo_sale.slug,
            "title" => mondeo_sale.title,
            "photo_path" => Rails.application.routes.url_helpers.rails_blob_path(mondeo_sale.photo, only_path: true)
          },
          "type" => "sale",
          "id" => mondeo_sale.id.to_s
        }
      )
    end
  end

  describe 'GET /api/sales/:slug' do
    it 'returns a sale' do
      svc = SaleCreator.new({
        title: Faker::Vehicle.make_and_model,
        description: Faker::Lorem.paragraph,
        price: Random.rand(100_000)
      })
      sale = svc.call.sale
      photo = open(Rails.root.join('spec/fixtures/files/example_car.jpg'))
      sale.photo.attach(io: photo, filename: 'photo.jpg')

      get "/api/sales/#{sale.slug}"

      expect(JSON.parse(response.body)['data']).to eq(
        {
          "attributes" => { 
            "description" => sale.description,
            "price" => sale.price,
            "slug" => sale.slug,
            "title" => sale.title,
            "photo_path" => Rails.application.routes.url_helpers.rails_blob_path(sale.photo, only_path: true)
          },
          "type" => "sale",
          "id" => sale.id.to_s
        }
      )
    end
  end

  after(:all) do
    FileUtils.rm_rf(Rails.root.join('tmp', 'storage'))
  end
end
