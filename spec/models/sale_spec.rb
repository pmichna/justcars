require 'rails_helper'

RSpec.describe Sale do
  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_numericality_of(:price).only_integer.is_greater_than_or_equal_to(0) }
    it { should validate_presence_of(:slug) }
    it { should validate_length_of(:slug).is_at_most(60) }
  end
end
