Rails.application.routes.draw do
  scope :api, defaults: { format: :json } do
    resources :sales, only: [:create, :show, :index], param: :slug
  end
end
